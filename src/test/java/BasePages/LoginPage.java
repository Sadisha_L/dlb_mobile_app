package BasePages;

import BaseClasses.BaseSetUp;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class LoginPage extends BaseSetUp {
    @CacheLookup
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.widget.ImageView")
    public WebElement shpcart;

    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Sign in']")
    public WebElement signInBtn;

    @CacheLookup

    @FindBy(xpath = "//android.widget.EditText[@index='4']")
    public WebElement passcode;

    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Confirm']")
    public WebElement confirmBtn;


    @CacheLookup

    @FindBy(xpath = "//android.view.ViewGroup[@index='12']")
    public WebElement passcode1;

    @CacheLookup
    @FindBy(xpath = "//android.view.ViewGroup[@index='3']")
    private WebElement loginPopUp;


    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Close']")
    public WebElement SettingsBtn;


    @CacheLookup
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Get Started']")
    public WebElement getstartBtn;


    @CacheLookup
    @FindBy(className = "//android.widget.EditText[@index='6']")
    public WebElement SettingsBtn2;


    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Confirm']")
    public WebElement SignInBtn;

    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Confirm' and @index='1']")
    public WebElement agreementConfirm;



    @CacheLookup
    @FindBy(xpath = "//android.widget.CheckBox[@index='2']")
    public WebElement checkBox;


    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Skip this Step']")
    public WebElement imageSkiptxt;


    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Skip this Step']")
    public WebElement bankSkiptxt;

    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Change phone number']")
    public WebElement chnagemobileNumber;


    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@index='1']")
    public WebElement signuppageTitle;


    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@index='0']")
    public WebElement signuppage;



    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Yes']")
    public WebElement exitMsg;
    public void tapOnsigninBtn(String value) throws InterruptedException {
        BasePage.scrollTo(value);
        signInBtn.click();
    }


    public void enterIDNumber(String s) {
        passcode.click();
        passcode.sendKeys(s);
    }

    public void tapOnSubmitBtn() throws InterruptedException {
        confirmBtn.click();
        Thread.sleep(2000);


    }

    public void enterPassCode1(String s) {
        for (int i = 1; i <= 4; i++) {
            System.out.println("Enter number 6 as time =" + i);
            BasePage.element_execute_javaScirpt(passcode1);
        }

    }

    public HomePage close_welcomepopUp() throws InterruptedException {
        Thread.sleep(3000);
        SettingsBtn.click();


        BasePage.capture_ScreenShot3();
        return PageFactory.initElements(driver, HomePage.class);
    }


    public LoginPage tapontheGetStartedBtn() {


        TouchAction touchAction2 = new TouchAction(driver);
        touchAction2.tap(PointOption.point(269, 989)).perform();

        /*  WebElement editText = driver.findElement(By.xpath("//android.widget.TextView[@index='1']"));
          editText.click();*/




        return this;
    }

    public void enterFirstname(String sadisha) {

        List<WebElement> editText = driver.findElements(By.className("android.widget.EditText"));
        System.out.println("Size of the array" + editText.size());
        editText.get(0).sendKeys(sadisha);


    }

    public void enterLastname(String test) {
        List<WebElement> editText = driver.findElements(By.className("android.widget.EditText"));
        System.out.println("Size of the array" + editText.size());
        editText.get(1).sendKeys(test);

    }

    public void enterNicNumber() {

        String number = BasePage.genarateNICnumber();
        List<WebElement> editText = driver.findElements(By.className("android.widget.EditText"));
        System.out.println("Size of the array" + editText.size());
        editText.get(2).sendKeys(number);

    }

    public void enterMobileNumber(String s) {

        List<WebElement> editText = driver.findElements(By.className("android.widget.EditText"));
        System.out.println("Size of the array" + editText.size());
        editText.get(3).sendKeys(s);
    }

    public void enterEmailAddress(String s) {
        List<WebElement> editText = driver.findElements(By.className("android.widget.EditText"));
        System.out.println("Size of the array" + editText.size());
        editText.get(4).sendKeys(s);
    }

    public void tapOntheSign() {


        TouchAction touchAction2 = new TouchAction(driver);
        touchAction2.tap(PointOption.point(301, 1198)).perform();


    }

    public void enterOtpnumber(String s) throws InterruptedException {
        Thread.sleep(2000);
        List<WebElement> editText = driver.findElements(By.className("android.widget.EditText"));
        System.out.println("Size of the array" + editText.size());
        for (int i = 0; i <= 5; i++) {
            editText.get(i).sendKeys(s);
        }
    }

    public void confirmOtpBtn() throws InterruptedException {
        Thread.sleep(2000);
        SignInBtn.click();
        BasePage.capture_ScreenShot3();
    }

    public void enterpinNumber(String s) throws InterruptedException {
        Thread.sleep(4000);
        List<WebElement> editText2 = driver.findElements(By.className("android.widget.EditText"));
        System.out.println("pin number array size is  = " + editText2.size());
        for (int i = 0; i <= 7; i++)
        {
            System.out.println("number of executing = "+i);


            try {
                new WebDriverWait(driver, 2).until(ExpectedConditions.elementToBeClickable(editText2.get(i)));
                editText2.get(i).click();
                editText2.get(i).sendKeys(s);
                System.out.println("number of passed elements to array  = "+i);
            }
            catch(Exception ex)
            {

                System.out.println("WebElement not found  ");
                System.out.println("not found count of array = "+i);
            }


        }



    }

    public void waitForWebElement(WebElement weblement)
    {
        try {
            new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOf(weblement));
        }
        catch(Exception ex){

            System.out.println("WebElement "+weblement.toString() +" not found");
        }

    }



    public void confirmPinNumber() throws InterruptedException {

        Thread.sleep(2000);
        BasePage.element_execute_javaScirpt(SignInBtn);


    }

    public void verifyCheckbox() throws InterruptedException {
        Thread.sleep(3000);
        BasePage.element_execute_javaScirpt(checkBox);

    }

    public void verifyConfirmBtn() throws InterruptedException {
        Thread.sleep(2000);
        BasePage.element_execute_javaScirpt(agreementConfirm);
    }

    public void skipnicImage() {
        BasePage.element_execute_javaScirpt(imageSkiptxt);
        //imageSkiptxt.click();
    }

    public void skipBankAccount() throws InterruptedException {
        Thread.sleep(1000);
        BasePage.element_execute_javaScirpt(bankSkiptxt);
       // bankSkiptxt.click();
    }

    public void purchaseGIdTickets() {

        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.widget.ImageView")));
        WebElement element = driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.widget.ImageView"));




        System.out.println("inside the loop to purchase :");
        System.out.println("inside the loop to purchase :");

        System.out.println(" Value of the second element is is  ==  " + shpcart.getLocation().getX());
        System.out.println(" Value of the second element is is  ==  " + shpcart.getLocation().getY());


        for (int i = 1; i <= 6; i++)
        {

            try {
                new WebDriverWait(driver, 4).until(ExpectedConditions.elementToBeClickable(shpcart));

                System.out.println("added ticket count number jjbjbkjkj = " + i);
                TouchAction touchAction2 = new TouchAction(driver);
                //  touchAction2.tap(PointOption.point(360, 616)).perform();
                touchAction2.tap(PointOption.point(370, 595)).perform();
                Thread.sleep(3000);

            }
            catch(Exception ex)
            {

                System.out.println("WebElement not found  exit loop kkkk ");

            }



        }
    }

    public void clikOnmobileNumberchange() {

        chnagemobileNumber.click();
    }

    public String verifySignupdetails() {
        return signuppageTitle.getText();
    }

    public void waitforElementtobepresent(String sign_in) {
        BasePage.apply_fluent_wait(signuppage,sign_in);
    }

    public void verifyExitMsg() {
        exitMsg.click();
    }
}
