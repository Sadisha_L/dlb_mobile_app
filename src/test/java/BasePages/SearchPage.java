package BasePages;

import BaseClasses.BaseSetUp;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static BaseClasses.StaticData.folderPath2;

public class SearchPage extends BaseSetUp {
    @CacheLookup
    @FindBy(xpath = "//android.view.View[@resource-id='android:id/month_view' and @index='1']")
    public WebElement startdate;

    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Won tickets']")
    public WebElement enddate;


    @CacheLookup
    @FindBy(xpath = "//android.view.ViewGroup[@index='5']")
    public WebElement search;



    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Draw Date']")
    public WebElement drawdate;


    @CacheLookup
    @FindBy(xpath = "//android.widget.Button[@text='OK']")
    public WebElement okbutton;


    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Advanced search']")
    public WebElement advancedSearchbtn;

    public SearchPage clickOnStartDate()

    {
        List<WebElement> editText = driver.findElements(By.xpath("//android.widget.TextView[@index='1']"));
        System.out.println("Size of the numbers of   = " + editText.size());


        for(int i=0;i<editText.size();i++)
        {
            System.out.println("Value of the elements areee = " + editText.get(i).getText());

        }
        editText.get(2).click();
        return this;
    }


    public SearchPage clickOnenedDate() {

        List<WebElement> editText = driver.findElements(By.xpath("//android.widget.TextView[@index='1']"));
        System.out.println("Size of the numbers of   = " + editText.size());
        editText.get(3).click();
        driver.findElementByXPath("//android.widget.Button[@index='1']").click();

        return this;
    }

    public SearchPage clickONSearchIcon() throws InterruptedException {
        Thread.sleep(5000);
        search.click();
        return this;
    }

    public void selectFromDate() throws InterruptedException {


        startdate.click();

        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd");

        String currentDateTime = format.format(date);
        System.out.println("System date is ="+currentDateTime);
        driver.findElementByXPath("//android.widget.Button[@index='1']").click();
        Thread.sleep(70000);
        BasePage.capture_ScreenShot3();
    }

    public SearchPage selectdrawdate() {
        drawdate.click();
        okbutton.click();
        return this;
    }

    public SearchPage selectZodiacsign() throws InterruptedException {
        Thread.sleep(1000);
        List<WebElement> editText = driver.findElements(By.xpath("//android.widget.EditText[@index='1']"));
        System.out.println("Size of the numbers of   = " + editText.size());
        editText.get(0).click();

        Thread.sleep(3000);
        WebElement element=driver.findElement(By.xpath("//android.view.ViewGroup[@index='3']"));
        element.click();
        return this;
    }

    public SearchPage enterpreferrednumber() {
        List<WebElement> editText = driver.findElements(By.xpath("//android.widget.EditText[@index='1']"));
        System.out.println("Size of the numbers of   = " + editText.size());
        editText.get(1).click();
        editText.get(1).sendKeys("10");
        driver.pressKey(new KeyEvent(AndroidKey.ENTER));


        return this;

    }

    public SearchPage clickOnAdvancedSearchBtn() {
        advancedSearchbtn.click();
        return this;
    }
}
