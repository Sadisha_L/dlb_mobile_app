package BasePages;

import BaseClasses.BaseSetUp;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class BankPage extends BaseSetUp {


    private Select provinceselect;
    @CacheLookup
    @FindBy(xpath = "//android.widget.EditText[@index='2']")
    public WebElement Nickname;

    @CacheLookup
    @FindBy(xpath = "//android.widget.EditText[@index='6']")
    public WebElement BankAccountNumber;

    @CacheLookup
    @FindBy(xpath = "//android.view.ViewGroup[@index='10']")
    public WebElement BankfromList;

    @CacheLookup
    @FindBy(xpath = "//android.view.ViewGroup[@index='13']")
    public WebElement sampathbank;

    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Savings account']")
    public WebElement BankType;

    @CacheLookup
    @FindBy(xpath = "//android.widget.CheckBox[@index='14']")
    public WebElement BankAccountAsprimary;

    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Add bank account']")
    public WebElement BankAccountBtn;


    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@index='0' and @text='Yes']")
    private WebElement accountconfirmbtn;

    @FindBy(xpath = "//android.view.ViewGroup[@index='4']")
    private WebElement confirnew;

    @CacheLookup

    @FindBy(xpath = "//android.widget.EditText[@index='4']")
    private WebElement amountToVerify;


    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@text='Confirm']")
    private WebElement amountToconfirm;


    public BankPage addAccountNickname(String dlb_account) {
        System.out.println("navigate to bank account details page");
        Nickname.click();
        Nickname.sendKeys(dlb_account);
        driver.pressKey(new KeyEvent(AndroidKey.BACK));
        return this;
    }

    public BankPage addBankAccountNumber(String s) throws InterruptedException {

        BankAccountNumber.click();
        BankAccountNumber.sendKeys(s);
        driver.pressKey(new KeyEvent(AndroidKey.BACK));
        return this;
    }

    public BankPage selectBankfromList() throws InterruptedException {

        Thread.sleep(1500);
         BankfromList.click();
        List<WebElement> editText = driver.findElements(By.xpath("//android.widget.TextView[@text='Select Bank Identity']"));
        System.out.println("Size of the array = " + editText.size());
        WebElement yourElement1 = editText.get(0);
        System.out.println("    Text value for the web elements are    ==  " + yourElement1.getText());
        editText.get(0).click();
        Thread.sleep(1000);
        WebElement editText4 = driver.findElement(By.xpath("//android.widget.TextView[@text='BOC']"));
        editText4.click();
        return this;
    }

    public BankPage selectBankType(String value) throws InterruptedException {

        sampathbank.click();

        Thread.sleep(1000);
        BankType.click();
        Thread.sleep(1000);

        return this;
    }

    public BankPage setBankAccountAsprimary() throws InterruptedException {
        Thread.sleep(1000);
        BankAccountAsprimary.click();
        return this;
    }

    public BankPage taponBankAccountBtn() throws InterruptedException {
        Thread.sleep(1000);
        BankAccountBtn.click();

        return this;
    }

    public BankPage confirmBankAccount() {
        accountconfirmbtn.click();

        return this;
    }

    public void verifyBankAccount() throws InterruptedException {

        List<WebElement> editText = driver.findElements(By.xpath("//android.view.ViewGroup[@index='0']"));
        System.out.println("Size of the array = " + editText.size());
        WebElement yourElement1 = editText.get(0);
        System.out.println("    Text value for the web elements are    ==  " + yourElement1.getText());
        BasePage.capture_ScreenShot3();

    }

    public void accountVerification() {
        confirnew.click();
    }

    public void addAmountToverify() {
        amountToVerify.click();
        amountToVerify.sendKeys("100");
    }

    public void amountconfirmBtn() {
        amountToconfirm.click();
    }
}
