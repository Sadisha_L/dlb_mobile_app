package BasePages;

import BaseClasses.BaseSetUp;
import com.google.common.collect.ImmutableMap;
import io.appium.java_client.MobileBy;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.List;


public class HomePage extends BaseSetUp {

    @CacheLookup
    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.widget.ImageView")
    public WebElement shpcart;


    @CacheLookup
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='My Tickets']")
    public WebElement shoppincartBtnnext;


    @CacheLookup
    @FindBy(xpath = "//*android.widget.HorizontalScrollView[@index='2']")
    public WebElement outSide;

    @CacheLookup
    @FindBy(xpath = "//android.view.ViewGroup[1]/android.widget.ImageView[0]")
    public WebElement shoppincartBtn;

    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@text='Profile']")
    private WebElement profileIcon;


    @FindBy(xpath = "//android.widget.TextView[@text='your winnings']")
    private WebElement winnigs;


    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@text='Sign Out']")
    private WebElement signoutBtn;


    @CacheLookup

    @FindBy(xpath = "//android.view.ViewGroup[@index='4']")
    private WebElement confirmSignOut;


    // @FindBy(xpath = "//android.widget.TextView[@text='Sign Out' and @index='1']")
    @FindBy(xpath = "//android.view.ViewGroup[@index='6']")

    private WebElement SignOutButton;


    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@index='0']")
    private WebElement listviewBtn;


    @CacheLookup

    @FindBy(xpath = "//android.view.ViewGroup[@index='5']")
    private WebElement addTocartBtn;


    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@index='1' and @text='Add to cart']")
    private WebElement cartBtn;

    @CacheLookup

    @FindBy(xpath = "//android.view.ViewGroup[@index='2']")
    private WebElement bulkpurchaseBtn;


    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@text='Add new payment method +']")
    private WebElement paymentlink;


    @CacheLookup

    @FindBy(xpath = "//android.view.ViewGroup[@index='2' and @NAF='true']")
    private WebElement paymebankntlink;

    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@text='Select a game to buy tickets']")
    private WebElement searchIcon;


    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@index='1' and @text='Open cart']")
    private WebElement addcartBtn;


    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@index='4']")
    private WebElement totalAmount;


    @FindBy(xpath = "//android.widget.TextView[contains(@class='Sign Out')]")
    private WebElement signout;


    @FindBy(xpath = "//android.widget.TextView[@text='My Tickets']")
    private WebElement MyTicketsIcon;

    @FindBy(xpath = "//android.widget.TextView[@text='Results']")
    private WebElement ResultIcon;


    @FindBy(xpath = "//android.widget.TextView[@text='Scheduled purchase']")
    private WebElement stringValueShedule;

    @FindBy(xpath = "//android.widget.TextView[@text='Cart ticket purchase']")
    private WebElement stringValuecart;

    @FindBy(xpath = "//android.widget.TextView[@text='Bulk ticket purchase']")
    private WebElement stringValuebulk;



    @FindBy(xpath = "//android.widget.TextView[@text='Lagna Wasana' and @index='1']")
    private WebElement lagnaVasana;


    @FindBy(xpath = "//android.widget.TextView[@index='1']")
    private WebElement suceesmsg;

    @FindBy(xpath = "*//android.widget.TextView[1]//android.view.ViewGroup[0]")
    private WebElement profileEdit;



    @FindBy(xpath = "//android.widget.TextView[@index='1']")
    private WebElement laganwasanatext;

    @FindBy(xpath = "//android.widget.TextView[@text='Search for your ticket']")
    private WebElement applyfilter;

    @FindBy(xpath = "//android.widget.TextView[@text='Review Terms & Conditions']")
    private WebElement terms;


    @FindBy(xpath = "//android.widget.TextView[@index='2']")
    private WebElement notifications;



    @FindBy(xpath = "//android.widget.TextView[@text='My Tickets']")
    private WebElement MyticketsIcon;


    public void ScrollDownToElement(String value) throws InterruptedException {
        BasePage.scrollTo(value);

    }

    public void purchase_tickets() throws InterruptedException {
        System.out.println("inside the loop to purchase :");
        System.out.println("inside the loop to purchase :");

      /*  System.out.println(" Value of the second element is is  ==  " + shpcart.getLocation().getX());
        System.out.println(" Value of the second element is is  ==  " + shpcart.getLocation().getY());*/


        for (int i = 1; i <= 13; i++) {

            try {
                // new WebDriverWait(driver, 4).until(ExpectedConditions.elementToBeClickable(shpcart));

                System.out.println("added ticket count number = " + i);
                TouchAction touchAction2 = new TouchAction(driver);
                touchAction2.tap(PointOption.point(370, 987)).perform();
                Thread.sleep(3000);

            } catch (Exception ex) {
                /*Thread.sleep(2000);
                new WebDriverWait(driver, 4).until(ExpectedConditions.elementToBeClickable(shpcart));
                System.out.println("except tick ticket count number = " + i);
                TouchAction touchAction2 = new TouchAction(driver);
                touchAction2.tap(PointOption.point(360, 616)).perform();*/
                System.out.println("WebElement not found  exit loop ");

            }


        }

    }


    public ShoppingCart navigate_To_ShoppingCart() {
        TouchAction touchAction2 = new TouchAction(driver);
        touchAction2.tap(PointOption.point(633, 136)).perform();
        return PageFactory.initElements(driver, ShoppingCart.class);

    }

    public HomePage tapOntheProfileIcon() {


        profileIcon.click();
        return this;
    }

    public HomePage tapOnSignoutBtn() throws InterruptedException {
        // driver.executeScript("mobile: performEditorAction", ImmutableMap.of("action", "Go"));

        try {
            new WebDriverWait(driver, 7).until(ExpectedConditions.elementToBeClickable(signout));
            System.out.println(" Value of the second element is is  ==  " + signout.getLocation().getX());
            System.out.println(" Value of the second element is is  ==  " + signout.getLocation().getY());


            TouchAction touchAction2 = new TouchAction(driver);
            touchAction2.tap(PointOption.point(302, 1134)).perform();
            Thread.sleep(1000);

        } catch (Exception ex) {

            System.out.println("WebElement not found  ");
            TouchAction touchAction2 = new TouchAction(driver);
            touchAction2.tap(PointOption.point(151, 1108)).perform();

        }

     /*   TouchAction touchAction2 = new TouchAction(driver);
        touchAction2.tap(PointOption.point(302, 1134)).perform();
        Thread.sleep(1000);*/

        return this;
    }

    public HomePage changeTolistView() {

        TouchAction touchAction2 = new TouchAction(driver);
        touchAction2.tap(PointOption.point(546, 446)).perform();
        return this;
    }

    public HomePage selectLoteriesToPurchase() throws InterruptedException {
        List<WebElement> editText = driver.findElements(By.className("android.widget.CheckBox"));
        System.out.println("Size of the array = " + editText.size());
        for (int i = 0; i <= 4; i++) {
            editText.get(i).click();
            Thread.sleep(1000);
        }
        return this;
    }

    public HomePage tapOnaddToCartBtn() throws InterruptedException {

        Thread.sleep(2000);
        cartBtn.click();
        return this;

    }

    public BulkPage tapOnThebulkPurchase() throws InterruptedException {
        // schduler
        // touchAction2.tap(PointOption.point(365, 737)).perform();


        TouchAction touchAction2 = new TouchAction(driver);
        // touchAction2.tap(PointOption.point(67, 753)).perform();
        touchAction2.tap(PointOption.point(67, 737)).perform();
        //touchAction2.tap(PointOption.point(67, 773)).perform();


       /* List<WebElement> editText = driver.findElements(By.xpath("//android.view.ViewGroup[@index='2']"));
        System.out.println("Size of the array to set bulk = " + editText.size());
        for (int i = 0; i < 4; i++)
        {
           // editText.get(i).click();
            Thread.sleep(1000);


            try {
                new WebDriverWait(driver, 4).until(ExpectedConditions.elementToBeClickable(editText.get(i)));
                editText.get(0).click();

                System.out.println("number of passed elements to array  = "+i);
            }
            catch(Exception ex)
            {

                System.out.println("WebElement not found   new ");
                System.out.println("not found count of array = "+i);
                System.out.println("number of passed elements to array  = "+editText.get(i).getText());
            }



        }
*/

        return PageFactory.initElements(driver, BulkPage.class);

    }


    public SchdulePage tapOnTheScedulle() throws InterruptedException {
        Thread.sleep(2000);
        TouchAction touchAction2 = new TouchAction(driver);
        touchAction2.tap(PointOption.point(365, 737)).perform();
        return PageFactory.initElements(driver, SchdulePage.class);
    }

    public HomePage clickOnAddNewpaymentLink() {
        paymentlink.click();
        return this;
    }

    public BankPage selectPaymentTypeAsBank() throws InterruptedException {
        List<WebElement> editText = driver.findElements(By.xpath("//android.view.ViewGroup[@index='2']"));
        System.out.println("Size of the array = " + editText.size());
        editText.get(0).click();
        System.out.println("select payment type as Bank Account in DLB :");
        return PageFactory.initElements(driver, BankPage.class);

    }

    public void tapOnnormalSearchImage() {
        searchIcon.click();
    }

    public void selectLottertyType() throws InterruptedException {
        Thread.sleep(10000);

        WebElement editText = driver.findElement(By.xpath("//android.widget.ImageView[@index='0']"));
        editText.click();
    }

    public void buyLottery() throws InterruptedException {
        Thread.sleep(10000);
        System.out.println("inside the lottery loop  = ");
        List<WebElement> editText = driver.findElements(By.xpath("//android.view.ViewGroup[@index='0']"));
        System.out.println("Size of the numbers of lotters are  = " + editText.size());

        for (int i = 5; i < (editText.size() - 30); i++) {
            WebElement yourElement = editText.get(i);
            System.out.println("    Text value for the web elements are    ==  " + yourElement.getText());
            System.out.println("    number count is     ==  " + i);
            yourElement.click();
            Thread.sleep(1000);
            BasePage.navigateTowinningelement();

            System.out.println("Total ticket amount === ");

        }


    }

    public void clickOnOpenCartBtn() {

        addcartBtn.click();
        System.out.println("Navigate to cart page: ");
    }

    public void confirmSignoutfunction() throws InterruptedException {
        Thread.sleep(1000);

        TouchAction touchAction2 = new TouchAction(driver);
        touchAction2.tap(PointOption.point(524, 736)).perform();
    }

    public HomePage scrollDown() {
        BasePage.scrollDown();
        return this;
    }

    public void navigatetOelement() {


        String text = "Sign Out";


        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"" + text + "\").instance(0))").click();

        // androidDriver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains("""+visibleText+""").instance(0))").click();

    }


    public void scrollingDownToelement() {
        String text = "Popular tickets today";


        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"" + text + "\").instance(0))");
    }

    public MyTicketsPage navigateToResultPage() {
        MyTicketsIcon.click();
        return PageFactory.initElements(driver, MyTicketsPage.class);
    }

    public SearchPage navigateToSearchPage() {
        ResultIcon.click();
        return PageFactory.initElements(driver, SearchPage.class);
    }

    public void scrolldowntotransaction() {
        String text = "Transaction History";
        driver.findElementByAndroidUIAutomator
                ("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"" + text + "\").instance(0))").click();

    }

    public void scrolldowntoSchduletransaction() {

        String text = "Scheduled purchase";
        driver.findElementByAndroidUIAutomator
                ("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"" + text + "\").instance(0))");

    }

    public void scrolldowntocarttransaction() {

        String text = "Cart ticket purchase";
        driver.findElementByAndroidUIAutomator
                ("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"" + text + "\").instance(0))");

    }

    public void scrolldowntobulktransaction() {

        String text = "Bulk ticket purchase";
        driver.findElementByAndroidUIAutomator
                ("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"" + text + "\").instance(0))");

    }

    public String getTransactionvalue() {

        System.out.println("Value of the element is =" + stringValueShedule.getText());
        return stringValueShedule.getText();
    }

    public String getTransactionvaluecart() {
        System.out.println("Value of the element is =" + stringValuecart.getText());
        return stringValuecart.getText();
    }

    public String getTransactionvaluebulk() {
        System.out.println("Value of the element is =" + stringValuebulk.getText());
        return stringValuebulk.getText();
    }

    public void scrollDownToText() {
        String text = "Popular ticket brand today";
        driver.findElementByAndroidUIAutomator
                ("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"" + text + "\").instance(0))");

    }

    public void navigatetoLagnavasanalotteryTickets() throws InterruptedException {
        lagnaVasana.click();
        Thread.sleep(5000);
    }

    public void waitUntillTicketsLoad(String value)
    {
        BasePage.apply_fluent_wait(suceesmsg,value);
    }

    public String verifyLagnavasanaIsloaded() throws InterruptedException {
        Thread.sleep(2000);
        List<WebElement> editText = driver.findElements(By.xpath("//android.view.ViewGroup[@index='0']"));
        System.out.println("Size of the numbers of lotters are  = " + editText.size());
         String Value="";
        for (int i = 0; i < (editText.size()); i++) {
            WebElement yourElement = editText.get(i);
            System.out.println("    Text value for the web elements are    ==  " + yourElement.getText());
             Value   = driver.findElementByXPath("//android.widget.TextView[@text='Lagna Wasana']").getText();
            if(Value.equals("Lagna Wasana"))
            {
                System.out.println("Ticket validated");
                break;
            }
            else
            {
                System.out.println("ticket tpe is invalid ");
            }


        }
    return Value;
    }


    public ProfilePage clickOneditIcon() throws InterruptedException {
        Thread.sleep(2000);

        TouchAction touchAction2 = new TouchAction(driver);
        touchAction2.tap(PointOption.point(612, 100)).perform();
        Thread.sleep(1000);

        return PageFactory.initElements(driver, ProfilePage.class);
    }

    public SearchPage clickonSearchforYourTicket()
    {
        applyfilter.click();
        return PageFactory.initElements(driver, SearchPage.class);
    }

    public HomePage navigate_To_terms_and_conditions() {
        String text = "Terms and conditions";
        driver.findElementByAndroidUIAutomator
                ("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"" + text + "\").instance(0))").click();

        return this;
    }

    public String verifyTermscontent() {
        return terms.getText();
    }

    public HomePage navigate_to_notifications() {

        List<WebElement> editText = driver.findElements(By.xpath("//android.widget.TextView[@index='0']"));
        System.out.println("Size of text elements are  = " + editText.size());

        for (int i = 0; i < (editText.size()); i++) {
            WebElement yourElement = editText.get(i);
            System.out.println("    Text value for the web elements are    ==  " + yourElement.getText()+"element number = " +i);
            editText.get(2).click();


        }


        return this;
    }

    public void waitforUItobeloaded(String value) {
        BasePage.apply_fluent_wait(notifications,value);
    }

    public MyTicketsPage navigateToMyTickets()
    {
        MyticketsIcon.click();
        return PageFactory.initElements(driver, MyTicketsPage.class);

    }
}
