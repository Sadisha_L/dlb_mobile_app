package BasePages;

import BaseClasses.BaseSetUp;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ShoppingCart extends BaseSetUp {

    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@text='Checkout']")
    private WebElement checkOut;


    //android.widget.TextView[@text='Select a game to buy tickets']

    @FindBy(xpath = "//android.widget.TextView[@text='Complete payment']")
    private WebElement completeBtn;


    @FindBy(xpath = "//android.view.ViewGroup[@index='0']")
    private WebElement element;

    @FindBy(xpath = "//android.widget.TextView[@text='for your purchase']")
    private WebElement succesfullmsg;


    @FindBy(xpath = "//android.widget.TextView[@text='Done']")
    private WebElement closemsg;


    public ShoppingCart tapOncheckOutBtn() {
        BasePage.capture_ScreenShot3();
        checkOut.click();
        return this;
    }


    public ShoppingCart confirmpaymentBtn() {
        completeBtn.click();
        BasePage.capture_ScreenShot3();
        return this;
    }

    public void getElement() throws InterruptedException {

        List<WebElement> allelemnts2 = driver.findElementsByXPath("//android.widget.TextView[@index='0']");
        int a = allelemnts2.size();
        System.out.println("Size of web element is   ==  " + a);
        for (int i = 10; i <= a; i++) {
            System.out.println("    Value for the element     ==  " + i);
            Thread.sleep(1000);
            WebElement yourElement1 = allelemnts2.get(0);
            WebElement yourElement2 = allelemnts2.get(1);
            System.out.println("    Text value for the web elements are    ==  " + yourElement1.getText());
            System.out.println("    Text value for the web elements are    ==  " + yourElement2.getText());

        }
    }

    public String succesfullMsg() {
        return succesfullmsg.getText();

    }

    public void closeMessage() {
        closemsg.click();
    }
}
