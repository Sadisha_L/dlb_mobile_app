package BasePages;

import BaseClasses.BaseSetUp;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class BulkPage extends BaseSetUp {


    @CacheLookup

    @FindBy(xpath = "//android.widget.EditText[@text='5']")
    private WebElement ticketCount;


    @CacheLookup

    //@FindBy(xpath = "//android.widget.TextView[@index='1' and @text='Purchase']")


    @FindBy(xpath = "//android.widget.TextView[@index='1' and @text='Purchase']")
    private WebElement bulkPurchasebtn;


    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@text='Number of tickets']")
    private WebElement clickOutSide;


    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@text='Yes']")
    private WebElement confirmBtn;


    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@text='Complete payment']")
    private WebElement paymentcompleteBtn;


    public BulkPage changeTicketCount(String s) throws InterruptedException {
        Thread.sleep(2000);
        try {
            new WebDriverWait(driver, 2).until(ExpectedConditions.elementToBeClickable(ticketCount));
            System.out.println("number of passed elements to array ///////  = ");
            ticketCount.click();
            ticketCount.clear();
            ticketCount.sendKeys(s);
            System.out.println("number of passed elements to array  = ");
        }
        catch(Exception ex)
        {

            System.out.println("WebElement not found  ");

        }

        driver.pressKey(new KeyEvent(AndroidKey.BACK));
        return this;
    }


    public BulkPage tapOnBulkpurcaseBtn() throws InterruptedException {
       /* clickOutSide.click();
        Thread.sleep(4000);*/
      //  BasePage.element_execute_javaScirpt(bulkPurchasebtn);
        // bulkPurchasebtn.click();


      bulkPurchasebtn.click();


       /* String text="Purchase";



        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+text+"\").instance(0))").click();
*/
        return this;
    }

    public BulkPage tapOnthebulkConfirmBtn() {
        confirmBtn.click();
        return this;
    }

    public BulkPage tapOnPaymentCompleteBtn() {
        paymentcompleteBtn.click();
        return this;
    }
}
