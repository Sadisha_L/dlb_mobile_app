package BasePages;

import BaseClasses.BaseSetUp;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class ProfilePage extends BaseSetUp {

    @CacheLookup
    @FindBy(xpath = "//android.widget.EditText[@index='9']")
    public WebElement firstname;


    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Yes']")
    public WebElement confirmpopUp;


    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@index='1']")
    public WebElement successmsg;


    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Got it']")
    public WebElement gotitMsg;


    @CacheLookup
    @FindBy(xpath = "//android.widget.Button[@text='DENY']")
    public WebElement denybtn;

    public ProfilePage edit_firstName(String value) throws InterruptedException {
        firstname.click();
        firstname.clear();
        Thread.sleep(1000);
        firstname.sendKeys(value);
        driver.pressKey(new KeyEvent(AndroidKey.BACK));
        return this;
    }

    public ProfilePage scrolldowntoElement() {
        String text = "Update";
        driver.findElementByAndroidUIAutomator
                ("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"" + text + "\").instance(0))").click();

        return this;
    }

    public ProfilePage confrimrUpdatemsg() {
        confirmpopUp.click();
        return this;
    }

    public String verifyUpdateisSuccesfull() {
        return successmsg.getText();
    }

    public ProfilePage confirmgotItMsg() {
        BasePage.capture_ScreenShot3();
        gotitMsg.click();
        return this;
    }

    public ProfilePage confirmImagedenyButton() {
        denybtn.click();
        return this;
    }
}
