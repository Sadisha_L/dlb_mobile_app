package BasePages;

import BaseClasses.BaseSetUp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;


import java.util.List;

public class MyTicketsPage extends BaseSetUp {

    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Won tickets']")
    public WebElement wontickets;


    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='You have price']")
    public WebElement ticketTag;


    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Redeem']")
    public WebElement redeamproze;

    @CacheLookup
    @FindBy(xpath = "//android.view.ViewGroup[@index='0']")
    public WebElement selectBank;

    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='This ticket will be drawn on']")
    public WebElement notdrawnticket;


    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Congratulations']")
    public WebElement winticket;

    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Drew tickets']")
    public WebElement drawtickets;

    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='To be drawn']")
    public WebElement tobeDrawn;

    @CacheLookup
    @FindBy(xpath = "//android.widget.TextView[@text='Won tickets']")
    public WebElement wintickets;

    public MyTicketsPage openWonicketsTab() {
        wontickets.click();
        return this;
    }

    public MyTicketsPage scrolldownAndSelectwonticket() throws InterruptedException {

        System.out.println("inside the lottery loop  = " );
        List<WebElement> editText = driver.findElements(By.xpath("//android.view.ViewGroup[@index='0']"));
        System.out.println("Size of the numbers of lotters are  = " + editText.size());


            String text="You have price";
            driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+text+"\").instance(0))").click();



        System.out.println("exit loop was successfull ");

        return this;
    }

    public MyTicketsPage redeamPrize() throws InterruptedException {
        redeamproze.click();

        return this;
    }

    public MyTicketsPage clamWinnng() throws InterruptedException {
        System.out.println("Selecet bank function executed ");
        Thread.sleep(8000);

        List<WebElement> editText = driver.findElements(By.xpath("//android.widget.TextView[@index='3']"));
        System.out.println("Size of the numbers of lotters are  = " + editText.size());

        editText.get(3).click();
        return this;
    }

    public void verify_tickets_as_not_drwan()
    {

        String text="Not drawn yet";
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+text+"\").instance(0))").click();


    }

    public void verify_ticket_has_won() {
        String text="You have price";
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+text+"\").instance(0))").click();
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\"view Results\").instance(0))").click();

    }

    public String getVerifyMsg() {

        System.out.println("Given value for test is="+notdrawnticket.getText());
        return notdrawnticket.getText();
    }

    public String getVerifyMsgwin() {
        System.out.println("Given value for test is="+winticket.getText());
        return winticket.getText();
    }

    public void navigate_to_draw_tickets()
    {
        drawtickets.click();
    }

    public void navigate_to_toBe_drawn() {
        tobeDrawn.click();
    }

    public void navigate_to_toBe_won() {
        wintickets.click();
    }
}
