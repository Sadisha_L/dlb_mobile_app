package BasePages;

import BaseClasses.BaseSetUp;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.HashMap;
import java.util.function.Function;

import static BaseClasses.StaticData.folderPath2;

public class BasePage extends BaseSetUp {

    public BasePage(AndroidDriver driver1) {
        this.driver = driver1;
    }


    public static void element_execute_javaScirpt(final WebElement element) {
        try {
            element.click();
        } catch (Exception e) {
            JavascriptExecutor executor = driver;
            executor.executeScript("arguments[0].click();", element);

        }

    }



    public static void scrollTo(String text)
    {
        driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+text+"\").instance(0))");
    }



    public static void capture_ScreenShot3()
    {
        String location=folderPath2+"\\";
        String timeStamp1 = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());

        TakesScreenshot ts = driver;

        //capture screenshot as output type FILE
        File file = ts.getScreenshotAs(OutputType.FILE);

        try {
            //save the screenshot taken in destination path
            FileUtils.copyFile(file, new File(location+ timeStamp1 +".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(" ==  the login page screenshot is taken");

    }





    public static String genarateNICnumber()
    {
        int length = 8;
        boolean useLetters = false;
        boolean useNumbers = true;
        System.out.println("Test");
        String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
        System.out.println("System genarated String value is  === "+generatedString);
        String nicNum="8"+generatedString+"V";
        return nicNum;
    }

    public static boolean swipeFromUpToBottom()
    {
        try {
            JavascriptExecutor js = driver;
            HashMap<String, String> scrollObject = new HashMap<>();
            scrollObject.put("direction", "up");
            js.executeScript("mobile: scroll", scrollObject);
            System.out.println("Swipe up was Successfully done.");
        }
        catch (Exception e)
        {
            System.out.println("swipe up was not successfull");
        }
        return false;
    }


    public static void uiSelectByVisibleText(Select uiElement , String value1)
    {
        uiElement.selectByVisibleText(value1);
    }


    public static void scrollDown(){
        Dimension dimension = getDriver().manage().window().getSize();
        int scrollStart = (int) (dimension.getHeight() * 0.5);
        int scrollEnd = (int) (dimension.getHeight() * 0.2);

        new TouchAction(getDriver())
                .press(PointOption.point(0, scrollStart))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
                .moveTo(PointOption.point(0, scrollEnd))
                .release().perform();
    }


    public static void scrollNClick(WebElement el){
        int retry = 0;
        while(retry <= 5){
            try{
                el.click();
                break;
            }catch (org.openqa.selenium.NoSuchElementException e){
                scrollDown();
                retry++;
            }
        }
    }

    public static void navigateTowinningelement()
    {

    }

/*
    public static void scrollNClick(By listItems, String Text){
        boolean flag = false;

        while(true){
            for(WebElement el: AppDriver.getDriver().findElements(listItems)){
                if(el.getAttribute("text").equals(Text)){
                    el.click();
                    flag=true;
                    break;
                }
            }
            if(flag)
                break;
            else
                scrollDown();
        }
    }*/

    public static void apply_fluent_wait(WebElement element ,String textValue)
    {
        FluentWait<AndroidDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(180))
                .pollingEvery(Duration.ofMillis(500))
                .ignoring(java.util.NoSuchElementException.class)
                .ignoring(ElementClickInterceptedException.class);


        Function<AndroidDriver, WebElement> function = arg0 ->
        {
            WebElement element1 = element;
            String getTextOnPage = element1.getText();
            System.out.println("This is sample text for page ="+getTextOnPage);
            if(getTextOnPage.equals(textValue))
            {
                System.out.println(getTextOnPage);
                BasePage.capture_ScreenShot3();
                return element1;
            }else
            {
                System.out.println("FluentWait Failed , this is user ,this");
                return null;
            }
        };



        wait.until(function);
    }




}
