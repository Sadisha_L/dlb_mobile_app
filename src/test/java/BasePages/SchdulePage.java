package BasePages;

import BaseClasses.BaseSetUp;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;


public class SchdulePage extends BaseSetUp {

    @CacheLookup

    @FindBy(xpath = "//android.widget.Button[@index='2']")
    private WebElement bulkpurchaseBtn;


    @CacheLookup

    @FindBy(xpath = "//android.widget.EditText[@index='2']")
    private WebElement sheName;

    @CacheLookup

    @FindBy(xpath = "//android.view.ViewGroup[@index='14']")
    private WebElement frequency;


    @FindBy(xpath = "//android.view.ViewGroup[@content-desc='daily']")
    private WebElement daily;


    @CacheLookup

    @FindBy(xpath = "//android.view.ViewGroup[@index='16']")
    private WebElement clock;


    @CacheLookup

    @FindBy(xpath = "//android.widget.Button[@text='OK']")
    private WebElement clockOkBtn;

    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@index='4' and @text='Ticket brand']")
    private WebElement outside;


    @CacheLookup

    //@FindBy(xpath = "//android.view.ViewGroup[@index='20']")
    @FindBy(xpath = "//android.widget.TextView[@text='Select']")
    private WebElement paymentMethod;


    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@text='******8888']")
    private WebElement bank;


    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@index='1' and @text='Create Schedule']")
    private WebElement schdulebtn;


    @CacheLookup

    @FindBy(xpath = "//android.widget.TextView[@index='0' and @text='Yes']")
    private WebElement schduleconfirmbtn;


    public SchdulePage create_new_Schdulle() {

        bulkpurchaseBtn.click();
        return this;
    }

    public SchdulePage enternickName(String shedular) {

        sheName.click();
        sheName.sendKeys(shedular);
        driver.pressKey(new KeyEvent(AndroidKey.BACK));
        return this;
    }

    public SchdulePage selectTicketsEvery(String s) throws InterruptedException {
        outside.click();
        frequency.click();
        Thread.sleep(1000);

        daily.click();
        return this;
    }

    public SchdulePage selectpurchaseTime() throws InterruptedException {
        clock.click();
        Thread.sleep(1000);
        clockOkBtn.click();
        return this;

    }

    public SchdulePage selectPaymentMethod() throws InterruptedException {
        Thread.sleep(1000);
        paymentMethod.click();
        Thread.sleep(1500);
        bank.click();
        return this;

    }

    public void tapOnCreateSchduleBtn() {

        schdulebtn.click();
    }

    public void confirmSchduleBtn() throws InterruptedException {
        schduleconfirmbtn.click();
        Thread.sleep(3000);
        driver.pressKey(new KeyEvent(AndroidKey.BACK));
    }

    public void navigateToBack() throws InterruptedException {
       // driver.pressKey(new KeyEvent(AndroidKey.BACK));
        TouchAction touchAction2 = new TouchAction(driver);
        touchAction2.tap(PointOption.point(37, 80)).perform();
        Thread.sleep(1000);
    }
}
