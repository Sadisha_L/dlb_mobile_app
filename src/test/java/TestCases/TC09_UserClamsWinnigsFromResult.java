package TestCases;

import BaseClasses.BaseSetUp;
import BasePages.HomePage;
import BasePages.MyTicketsPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class TC09_UserClamsWinnigsFromResult extends BaseSetUp {
    final static Logger logger = Logger.getLogger(TC09_UserClamsWinnigsFromResult.class);

    @Test(priority = 1)
    public void clam_winnings() throws InterruptedException {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        login_to_system();
        MyTicketsPage mttpg =homePage.navigateToResultPage();
        mttpg.openWonicketsTab();
        Thread.sleep(10000);
        logger.info("This is info : user navigates to My lotteries page");
        mttpg.scrolldownAndSelectwonticket();
        logger.info("This is info : user selects win lotteries ");
        mttpg.redeamPrize();
        Thread.sleep(2000);
        mttpg.clamWinnng();
    }

    @Test(priority = 2)
    public void tear_down() throws InterruptedException {
        signout();
    }
    private void signout() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.tearDown();
    }


    private void login_to_system() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.login_to_app_with_Registered_User();

    }

}
