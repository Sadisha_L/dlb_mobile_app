package TestCases;

import BaseClasses.BaseSetUp;

import BasePages.HomePage;
import BasePages.SchdulePage;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class TC06_CreateTicketsSchedule extends BaseSetUp {

    final static Logger logger = Logger.getLogger(TC06_CreateTicketsSchedule.class);
    @Test(priority = 1)
    public void user_creates_shedule() throws InterruptedException {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        login_to_system();
        Thread.sleep(3000);
        SchdulePage shepg = homePage.tapOnTheScedulle();
        logger.info("This is info : user tap on create scheduler button   ");
        shepg.create_new_Schdulle();
        shepg.enternickName("shedular");
        shepg.selectTicketsEvery("Daily");
        shepg.selectpurchaseTime();
        homePage.ScrollDownToElement("Create Schedule");
        shepg.selectPaymentMethod();
        shepg.tapOnCreateSchduleBtn();
        logger.info("This is info : user creates new scheduler successfully   ");
        shepg.confirmSchduleBtn();
        Thread.sleep(2000);
        shepg.navigateToBack();



    }


    @Test(priority = 2)
    public void tearDown() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        Thread.sleep(3000);
        homepg.tapOntheProfileIcon();
        Thread.sleep(3000);
        logger.info("This is info : user tap on the profile icon thissss ");
        homepg.navigatetOelement();
        logger.info("This is info : user tap on the sign out button");
        Thread.sleep(1000);
        homepg.confirmSignoutfunction();
        logger.info("This is info : user tap on");


    }



    private void login_to_system() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.login_to_app_with_Registered_User();

    }




}
