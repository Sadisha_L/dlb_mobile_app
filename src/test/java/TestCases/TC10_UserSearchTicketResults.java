package TestCases;

import BaseClasses.BaseSetUp;
import BasePages.HomePage;
import BasePages.MyTicketsPage;
import BasePages.SearchPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class TC10_UserSearchTicketResults extends BaseSetUp {


    final static Logger logger = Logger.getLogger(TC10_UserSearchTicketResults.class);

    @Test(priority = 1)
    public void clam_winnings() throws InterruptedException {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        login_to_system();
        SearchPage serchPg = homePage.navigateToSearchPage();
        Thread.sleep(3000);
        serchPg.clickOnStartDate();
        serchPg.selectFromDate();
        serchPg.clickOnenedDate();
        serchPg.clickONSearchIcon();

    }

    @Test(priority = 2)
    public void tear_down() throws InterruptedException {
        signout();
    }

    private void signout() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.tearDown();
    }


    private void login_to_system() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.login_to_app_with_Registered_User();

    }


}
