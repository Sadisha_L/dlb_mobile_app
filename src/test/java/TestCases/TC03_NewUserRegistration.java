package TestCases;

import BaseClasses.BaseSetUp;
import BasePages.HomePage;
import BasePages.LoginPage;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class TC03_NewUserRegistration extends BaseSetUp {
    final static Logger logger = Logger.getLogger(TC03_NewUserRegistration.class);


    @Test(priority = 1)
    public void login_to_app_with_Registered_User() throws InterruptedException {
        LoginPage loginPg = PageFactory.initElements(driver, LoginPage.class);
        Thread.sleep(30000);
        System.out.println("execute in the session ");
        loginPg.tapontheGetStartedBtn();
        loginPg.enterFirstname("Sadisha");
        logger.info("This is info : user enter first name  ");
        loginPg.enterLastname("Test");
        logger.info("This is info : user enter last name");
        loginPg.enterNicNumber();
        logger.info("This is info : user enter nic number ");
        loginPg.enterMobileNumber("0770000003");
        logger.info("This is info : user enter mobile number ");
        loginPg.enterEmailAddress("abc@gmial.com");
        logger.info("This is info : user enter email address ");

        loginPg.tapOntheSign();
        loginPg.enterOtpnumber("1");
        Thread.sleep(2000);
        loginPg.confirmOtpBtn();
        loginPg.enterpinNumber("3");
        loginPg.confirmPinNumber();
        loginPg.verifyCheckbox();
        loginPg.verifyConfirmBtn();
        loginPg.skipnicImage();
        logger.info("This is info : user skip nic image  ");
        loginPg.skipBankAccount();
        logger.info("This is info : user skip bank account add ");
    }

    @Test(priority = 2)
    public void tearDown() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        Thread.sleep(3000);
        homepg.tapOntheProfileIcon();
        Thread.sleep(3000);
        logger.info("This is info : user tap on the profile icon thissss ");
        homepg.navigatetOelement();

        // homepg.tapOnSignoutBtn();
        logger.info("This is info : user tap on the sign out button");
        Thread.sleep(1000);
        homepg.confirmSignoutfunction();
        logger.info("This is info : user tap on the sign out button confirm button");
        // driver.closeApp();

    }


}
