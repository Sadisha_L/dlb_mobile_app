package TestCases;


import BaseClasses.BaseSetUp;
import BasePages.HomePage;
import BasePages.MyTicketsPage;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TC18_Verify_MyTickets_content_navigation extends BaseSetUp {


    @Test(priority = 1)
    public void verify_notification_all() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        login_to_system();
        MyTicketsPage myTicketsPage =homepg.navigateToMyTickets();
        myTicketsPage.verify_tickets_as_not_drwan();
        Thread.sleep(2000);
        Assert.assertEquals(myTicketsPage.getVerifyMsg(),"This ticket will be drawn on");
        driver.pressKey(new KeyEvent(AndroidKey.BACK));

    }
    @Test(priority = 2)
    public void verify_drawn_won() throws InterruptedException
    {
        MyTicketsPage myTicketsPage =PageFactory.initElements(driver, MyTicketsPage.class);
        myTicketsPage.verify_ticket_has_won();
        Thread.sleep(2000);
        Assert.assertEquals(myTicketsPage.getVerifyMsgwin(),"Congratulations");
        driver.pressKey(new KeyEvent(AndroidKey.BACK));
        System.out.println();
    }

    @Test(priority = 3)
    public void verify_draw_tickets() throws InterruptedException
    {
        MyTicketsPage myTicketsPage =PageFactory.initElements(driver, MyTicketsPage.class);
        myTicketsPage.navigate_to_draw_tickets();
        Thread.sleep(2000);

    }

    @Test(priority = 4)
    public void verify_to_be_drawn() throws InterruptedException
    {
        MyTicketsPage myTicketsPage =PageFactory.initElements(driver, MyTicketsPage.class);
        myTicketsPage.navigate_to_toBe_drawn();
        Thread.sleep(2000);

    }

    @Test(priority = 5)
    public void verify_won_tickets() throws InterruptedException
    {
        MyTicketsPage myTicketsPage =PageFactory.initElements(driver, MyTicketsPage.class);
        myTicketsPage.navigate_to_toBe_won();
        Thread.sleep(2000);

    }


    @Test(priority = 6)
    public void tear_down() throws InterruptedException
    {
        signout();
        Thread.sleep(2000);
    }

    private void signout() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.tearDown();
    }


    private void login_to_system() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.login_to_app_with_Registered_User();

    }

}
