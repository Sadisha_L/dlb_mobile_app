package TestCases;

import BaseClasses.BaseSetUp;
import BasePages.HomePage;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;



public class TC16_VerifyNotifictionUI extends BaseSetUp {
    final static Logger logger = Logger.getLogger(TC16_VerifyNotifictionUI.class);


    @Test(priority = 1)
    public void verify_notification_all() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        login_to_system();
        homepg.navigate_to_notifications();
        Thread.sleep(5000);
        // homepg.waitforUItobeloaded("You have won LKR1000000 from Lagna Wasana");

    }


    @Test(priority = 2)
    public void tear_down() throws InterruptedException {
        signout();
        Thread.sleep(4000);
        System.out.println("executed 1  ====");

        System.out.println("executed 2  ====");
      /* driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
       System.out.println("executed 3  ====");
       driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
       System.out.println("executed 4  ====");*/
    }

    private void signout() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.tearDown();
    }

    private void login_to_system() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.login_to_app_with_Registered_User();

    }
}
