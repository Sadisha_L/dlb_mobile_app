package TestCases;

import BaseClasses.BaseSetUp;
import BasePages.HomePage;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TC12_VerifyLoadingPopularBrands_Lagnawasanawa extends BaseSetUp {

    final static Logger logger = Logger.getLogger(TC12_VerifyLoadingPopularBrands_Lagnawasanawa.class);

    @Test(priority = 1)
    public void view_transaction() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        login_to_system();
        Thread.sleep(5000);
        homepg.scrollDownToText();
        homepg.navigatetoLagnavasanalotteryTickets();
        homepg.waitUntillTicketsLoad("Buy Tickets");
        Assert.assertEquals(homepg.verifyLagnavasanaIsloaded(),"Lagna Wasana");
        logger.info("This is info : lagna wasana lottry type successfully selected ");


    }


    @Test(priority = 2)
    public void tear_down() throws InterruptedException {
        signout();
    }

    private void signout() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.tearDown();
    }

    private void login_to_system() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.login_to_app_with_Registered_User();

    }
}
