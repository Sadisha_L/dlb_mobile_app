package TestCases;

import BaseClasses.BaseSetUp;
import BasePages.LoginPage;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TC17_VerifyMobileNumberChangein_SignUp extends BaseSetUp {

    final static Logger logger = Logger.getLogger(TC01_Registered_UserLogin.class);

@Test(priority = 1)
public void verify_mobile_number_change() throws InterruptedException {
    LoginPage loginPg = PageFactory.initElements(driver, LoginPage.class);
    Thread.sleep(3000);
    System.out.println("Element executed ===== in login ");

    System.out.println("Element executed 2 =====  in login ");
  //  loginPg.waitforElementtobepresent("Sign in");

    loginPg.tapontheGetStartedBtn();
    loginPg.enterFirstname("Sadisha");
    logger.info("This is info : user enter first name  ");
    loginPg.enterLastname("Test");
    logger.info("This is info : user enter last name");
    loginPg.enterNicNumber();
    logger.info("This is info : user enter nic number ");
    loginPg.enterMobileNumber("0770000003");
    logger.info("This is info : user enter mobile number ");
    loginPg.enterEmailAddress("abc@gmial.com");
    logger.info("This is info : user enter email address ");
    loginPg.tapOntheSign();
    Thread.sleep(1000);
    loginPg.clikOnmobileNumberchange();
    Thread.sleep(2000);
    Assert.assertEquals(loginPg.verifySignupdetails(),"Sign up to DLB Sweep");
    driver.pressKey(new KeyEvent(AndroidKey.BACK));
    Thread.sleep(1000);
    loginPg.verifyExitMsg();
    driver.resetApp();

}

}
