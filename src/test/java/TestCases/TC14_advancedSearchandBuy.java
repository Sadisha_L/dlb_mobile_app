package TestCases;

import BaseClasses.BaseSetUp;
import BasePages.HomePage;
import BasePages.SearchPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TC14_advancedSearchandBuy extends BaseSetUp {

    final static Logger logger = Logger.getLogger(TC14_advancedSearchandBuy.class);

    @Test(priority = 1)
    public void advan_search() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        login_to_system();
        SearchPage serchPg = homepg.clickonSearchforYourTicket();
        serchPg.selectdrawdate();
        serchPg.selectZodiacsign();
        // serchPg.enterpreferrednumber();
        serchPg.clickOnAdvancedSearchBtn();
        homepg.waitUntillTicketsLoad("Buy Tickets");
        Assert.assertEquals(homepg.verifyLagnavasanaIsloaded(), "Lagna Wasana");
        logger.info("This is info : lagna wasana lottry type successfully selected ");



    }


    @Test(priority = 2)
    public void tear_down() throws InterruptedException {
        signout();
    }

    private void signout() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.tearDown();
    }

    private void login_to_system() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.login_to_app_with_Registered_User();

    }
}
