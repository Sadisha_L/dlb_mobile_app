package TestCases;

import BaseClasses.BaseSetUp;
import BasePages.HomePage;
import BasePages.ProfilePage;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TC13_UserProfileEditFunction extends BaseSetUp {


    final static Logger logger = Logger.getLogger(TC13_UserProfileEditFunction.class);
    @Test(priority = 1)
    public void edit_user_profile() throws InterruptedException {
        login_to_system();
        Thread.sleep(3000);
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        homepg.tapOntheProfileIcon();
        ProfilePage propg = homepg.clickOneditIcon();
        propg.edit_firstName("Sadisha");
        logger.info("This is info : user edited first name input field");
        propg.scrolldowntoElement();
        propg.confrimrUpdatemsg();
        Thread.sleep(3000);
        Assert.assertEquals(propg.verifyUpdateisSuccesfull(), "Success!");
        propg.confirmgotItMsg();
        logger.info("This is info : user profile edit successfully completed");
        propg.confirmImagedenyButton();


    }

    @Test(priority = 2)
    public void tear_down() throws InterruptedException {
        signout();
    }

    private void signout() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.tearDown();
    }

    private void login_to_system() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.login_to_app_with_Registered_User();

    }
}
