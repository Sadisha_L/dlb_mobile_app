package TestCases;


import BaseClasses.BaseSetUp;
import BasePages.HomePage;
import BasePages.LoginPage;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class TC01_Registered_UserLogin extends BaseSetUp {

    final static Logger logger = Logger.getLogger(TC01_Registered_UserLogin.class);


    @Test(priority = 1)
    public void login_to_app_with_Registered_User() throws InterruptedException {
        LoginPage loginPg = PageFactory.initElements(driver, LoginPage.class);
        loginPg.tapOnsigninBtn("Sign in");
        logger.info("This is info : user tap on the sign up button ");
        loginPg.enterIDNumber("910362002V");
        logger.info("This is info : user enter id number  ");
        loginPg.tapOnSubmitBtn();
        loginPg.enterPassCode1("6");
        logger.info("This is info : user enter pin number on pin number UI ");
        loginPg.close_welcomepopUp();
    }

    @Test(priority = 2)
    public void tearDown() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
      /*  Thread.sleep(3000);*/
        homepg.tapOntheProfileIcon();
        Thread.sleep(1000);
        logger.info("This is info : user tap on the profile icon ");
        homepg.navigatetOelement();

       // homepg.tapOnSignoutBtn();
        logger.info("This is info : user tap on the sign out button");
        Thread.sleep(1000);
        homepg.confirmSignoutfunction();
        logger.info("This is info : user tap on the sign out button confirm button");


    }



}
