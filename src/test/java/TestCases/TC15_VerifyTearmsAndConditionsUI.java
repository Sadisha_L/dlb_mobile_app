package TestCases;

import BaseClasses.BaseSetUp;
import BasePages.HomePage;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TC15_VerifyTearmsAndConditionsUI extends BaseSetUp {

    final static Logger logger = Logger.getLogger(TC15_VerifyTearmsAndConditionsUI.class);

    @Test(priority = 1)
    public void verify_terms_and_conditions() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        login_to_system();
        homepg.tapOntheProfileIcon();
        homepg.navigate_To_terms_and_conditions();
        Thread.sleep(1500);
        Assert.assertEquals(homepg.verifyTermscontent(),"Review Terms & Conditions");
        driver.pressKey(new KeyEvent(AndroidKey.BACK));


    }


    @Test(priority = 2)
    public void tear_down() throws InterruptedException {
        signout();
    }

    private void signout() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.tearDown();
    }

    private void login_to_system() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.login_to_app_with_Registered_User();

    }
}
