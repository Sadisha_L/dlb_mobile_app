package TestCases;

import BaseClasses.BaseSetUp;
import BasePages.HomePage;
import BasePages.LoginPage;
import BasePages.ShoppingCart;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class TC02_Buy_Tickets_GridView extends BaseSetUp {
    final static Logger logger = Logger.getLogger(TC02_Buy_Tickets_GridView.class);

    @Test(priority = 1)
    public void user_buy_tickets_grid_view() throws InterruptedException {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        login_to_system();
        Thread.sleep(1000);
        homePage.scrollDown();
        Thread.sleep(7000);
        homePage.purchase_tickets();
        logger.info("This is info : user tap on the sign up button ");
        homePage.navigate_To_ShoppingCart();
        logger.info("This is info : navigate to shopping cart UI ");
        ShoppingCart cartpg = PageFactory.initElements(driver, ShoppingCart.class);
        // cartpg.getElement();
        cartpg.tapOncheckOutBtn();
        logger.info("This is info : user tap on the check out button ");
        cartpg.confirmpaymentBtn();
        logger.info("This is info : user confirm bank payment ");
        Assert.assertEquals(cartpg.succesfullMsg(),"for your purchase");
        cartpg.closeMessage();


    }

    @Test(priority = 2)
    public void tearDown() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        Thread.sleep(3000);
        homepg.tapOntheProfileIcon();
        Thread.sleep(3000);
        logger.info("This is info : user tap on the profile icon ");
        homepg.navigatetOelement();

        // homepg.tapOnSignoutBtn();
        logger.info("This is info : user tap on the sign out button");
        Thread.sleep(1000);
        homepg.confirmSignoutfunction();
        logger.info("Twwww");
        // driver.closeApp();

    }






    private void login_to_system() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.login_to_app_with_Registered_User();

    }


}
