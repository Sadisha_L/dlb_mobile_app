package TestCases;

import BaseClasses.BaseSetUp;

import BasePages.HomePage;
import BasePages.ShoppingCart;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import org.testng.annotations.Test;

public class TC08_BasicSearchandPurchase extends BaseSetUp {



    final static Logger logger = Logger.getLogger(TC08_BasicSearchandPurchase.class);

    @Test(priority = 1)
    public void user_buy_tickets_bulk() throws InterruptedException {
        login_to_system();
        Thread.sleep(5000);
        HomePage homepg= PageFactory.initElements(driver, HomePage.class);
        homepg.tapOnnormalSearchImage();
        homepg.selectLottertyType();
        logger.info("This is info : user selects lottery type to buy");
        Thread.sleep(15000);
        homepg.buyLottery();
        logger.info("This is info : user buys lotteries from list ");
        homepg.clickOnOpenCartBtn();
        ShoppingCart cartpg = PageFactory.initElements(driver, ShoppingCart.class);
        cartpg.tapOncheckOutBtn();
        cartpg.confirmpaymentBtn();
        Assert.assertEquals(cartpg.succesfullMsg(), "for your purchase");
        System.out.println();
        cartpg.closeMessage();


    }






    @Test(priority = 2)
    public void tearDown() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        Thread.sleep(3000);
        homepg.tapOntheProfileIcon();
        Thread.sleep(3000);
        logger.info("This is info : user tap on the profile icon thissss ");
        homepg.navigatetOelement();
        logger.info("This is info : usen out button");
        Thread.sleep(1000);
        homepg.confirmSignoutfunction();
        logger.info("This is info : user tap on the sign out button confirm button");


    }

    private void login_to_system() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.login_to_app_with_Registered_User();

    }


}
