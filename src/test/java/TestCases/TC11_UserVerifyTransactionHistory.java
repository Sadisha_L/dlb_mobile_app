package TestCases;

import BaseClasses.BaseSetUp;
import BasePages.HomePage;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TC11_UserVerifyTransactionHistory extends BaseSetUp {


    final static Logger logger = Logger.getLogger(TC11_UserVerifyTransactionHistory.class);

    @Test(priority = 1)
    public void view_transaction() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        login_to_system();
        homepg.tapOntheProfileIcon();
        homepg.scrolldowntotransaction();
        Thread.sleep(5000);


    }

    @Test(priority = 2)
    public void verify_Schedule_transaction() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        homepg.scrolldowntoSchduletransaction();
        Assert.assertEquals(homepg.getTransactionvalue(),"Scheduled purchase");

    }


    @Test(priority = 3)
    public void verify_cart_transaction() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        homepg.scrolldowntocarttransaction();
        Assert.assertEquals(homepg.getTransactionvaluecart(),"Cart ticket purchase");


    }

    @Test(priority = 4)
    public void verify_bulk_transaction() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        homepg.scrolldowntobulktransaction();
        Assert.assertEquals(homepg.getTransactionvaluebulk(),"Bulk ticket purchase");

    }

    @Test(priority = 5)
    public void tear_down() throws InterruptedException {
        signout();
    }

    private void signout() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.tearDown();
    }


    private void login_to_system() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.login_to_app_with_Registered_User();

    }


}
