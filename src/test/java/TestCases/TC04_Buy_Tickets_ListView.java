package TestCases;

import BaseClasses.BaseSetUp;
import BasePages.HomePage;
import BasePages.ShoppingCart;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class TC04_Buy_Tickets_ListView extends BaseSetUp {

    final static Logger logger = Logger.getLogger(TC04_Buy_Tickets_ListView.class);
    @Test(priority = 1)
    public void user_buy_tickets_listView() throws InterruptedException {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        login_to_system();
        Thread.sleep(3000);
        homePage.changeTolistView();
        Thread.sleep(7000);
        homePage.selectLoteriesToPurchase();
        logger.info("This is info : user select lotteris to purchase  ");
        homePage.ScrollDownToElement("Add to cart");
        homePage.tapOnaddToCartBtn();
        homePage.navigate_To_ShoppingCart();
        ShoppingCart cartpg = PageFactory.initElements(driver, ShoppingCart.class);
        cartpg.tapOncheckOutBtn();
        cartpg.confirmpaymentBtn();
        Assert.assertEquals(cartpg.succesfullMsg(), "for your purchase");
        cartpg.closeMessage();
    }



    @Test(priority = 2)
    public void tearDown() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        Thread.sleep(3000);
        homepg.tapOntheProfileIcon();
        Thread.sleep(3000);
        logger.info("This is info : user tap on the profile icon thissss ");
        homepg.navigatetOelement();
        logger.info("This is info : user tap on the sign out button");
        Thread.sleep(1000);
        homepg.confirmSignoutfunction();
        logger.info("This is info : ");


    }

    private void login_to_system() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.login_to_app_with_Registered_User();

    }


}
