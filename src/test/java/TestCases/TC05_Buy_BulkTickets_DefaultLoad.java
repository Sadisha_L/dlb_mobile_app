package TestCases;

import BaseClasses.BaseSetUp;
import BasePages.BulkPage;
import BasePages.HomePage;

import BasePages.ShoppingCart;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import org.testng.Assert;

import org.testng.annotations.Test;

public class TC05_Buy_BulkTickets_DefaultLoad extends BaseSetUp {

    final static Logger logger = Logger.getLogger(TC05_Buy_BulkTickets_DefaultLoad.class);

    @Test(priority = 1)
    public void user_buy_tickets_bulk() throws InterruptedException {
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        login_to_system();
        Thread.sleep(3000);

        homePage.scrollingDownToelement();

        BulkPage bulpg = homePage.tapOnThebulkPurchase();
        bulpg.changeTicketCount("6");
        logger.info("This is info : user change bulk purchase tickets count   ");
        bulpg.tapOnBulkpurcaseBtn();
        Thread.sleep(3000);
        logger.info("This is info : user tap on the bulk purchase button  ");
        bulpg.tapOnthebulkConfirmBtn();
        logger.info("This is info : user bulk purchase confirm button  ");
        Thread.sleep(3000);
        bulpg.tapOnPaymentCompleteBtn();
        ShoppingCart cartpg = PageFactory.initElements(driver, ShoppingCart.class);
        Assert.assertEquals(cartpg.succesfullMsg(), "for your purchase");
        cartpg.closeMessage();


    }




    @Test(priority = 2)
    public void tearDown() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        Thread.sleep(3000);
        homepg.tapOntheProfileIcon();
        Thread.sleep(3000);
        logger.info("This is info : user tap on the profile icon ////");
        homepg.navigatetOelement();

        // homepg.tapOnSignoutBtn();
        logger.info("This is info : user tap on the sign out button");
        Thread.sleep(1000);
        homepg.confirmSignoutfunction();
        logger.info("This is info : user tap on the sign out button confirm button");


    }

    private void login_to_system() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.login_to_app_with_Registered_User();

    }


}
