package TestCases;

import BaseClasses.BaseSetUp;
import BasePages.BankPage;
import BasePages.HomePage;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

public class TC07_UserAddBankAccount extends BaseSetUp {

    final static Logger logger = Logger.getLogger(TC07_UserAddBankAccount.class);

    @Test(priority = 1)
    public void user_add_bank_account() throws InterruptedException {
        login_to_system();
        Thread.sleep(3000);
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        homepg.tapOntheProfileIcon();
        homepg.clickOnAddNewpaymentLink();
        homepg.selectPaymentTypeAsBank();
        logger.info("This is info : user selects payment type as bank ");
        BankPage bankPg = PageFactory.initElements(driver, BankPage.class);
        bankPg.addAccountNickname("DLB Account");
        Thread.sleep(3000);
        bankPg.addBankAccountNumber("2333444444");
        bankPg.selectBankfromList();
        bankPg.selectBankType("Savings account");
        bankPg.setBankAccountAsprimary();
        bankPg.taponBankAccountBtn();
        logger.info("This is info : user creates bank account successfully  ");
        bankPg.confirmBankAccount();
        Thread.sleep(2000);
        bankPg.accountVerification();
        Thread.sleep(2000);
        bankPg.addAmountToverify();
        bankPg.amountconfirmBtn();
        Thread.sleep(2000);


    }

    @Test(priority = 2)
    public void tearDown() throws InterruptedException {
        HomePage homepg = PageFactory.initElements(driver, HomePage.class);
        Thread.sleep(1000);
        homepg.tapOntheProfileIcon();
        Thread.sleep(2000);
        logger.info("This is info : user tap on the profile icon thissss ");
        homepg.navigatetOelement();
        logger.info("This is info : user tap on the sign out button");
        Thread.sleep(1000);
        homepg.confirmSignoutfunction();
        logger.info("This is info : user tap on ttton");


    }

    private void login_to_system() throws InterruptedException {
        TC01_Registered_UserLogin obj1 = new TC01_Registered_UserLogin();
        obj1.login_to_app_with_Registered_User();

    }


}
