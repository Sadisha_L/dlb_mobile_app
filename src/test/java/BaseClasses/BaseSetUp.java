package BaseClasses;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class BaseSetUp {

    public static AndroidDriver driver = null;
    public static WebDriver driver2;
    private DesiredCapabilities capabilities = new DesiredCapabilities();

    private String port = "";
    private String serverIP = "127.0.0.1:4723";

    @BeforeSuite
    public void testSetUp() throws InterruptedException, MalformedURLException {
        initdriver();

    }


    public static AndroidDriver getDriver() {
        return driver;
    }

    private void initdriver() throws InterruptedException, MalformedURLException {
        File app = new File(Constants.apk_path);

        capabilities.setCapability("app", app.getAbsolutePath());
        capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
        capabilities.setCapability("deviceName", "device");
        capabilities.setCapability("udid", "JYNBB18422169709");
        capabilities.setCapability("platformVersion", "8.0.0");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("appPackage", "com.epic.dlb_sweep");
        capabilities.setCapability("appActivity", "com.epic.dlb_sweep.MainActivity");
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
        capabilities.setCapability("noReset", true);


        String serverUrl = "http://" + serverIP + port + "/wd/hub";


        try {
            System.out.println("Argument to driver object : " + serverUrl);
            driver = new AndroidDriver(new URL(serverUrl), capabilities);

        } catch (NullPointerException | MalformedURLException ex) {
            throw new RuntimeException("appium driver could not be initialised for device ");
        }
        System.out.println("Driver in initdriver is : " + driver);


        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        Thread.sleep(2000);

    }


}
