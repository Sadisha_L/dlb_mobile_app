package BaseClasses;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class App {

    private static Statement stmt;
    private static ResultSet results;

    public static void main(String[] args)
    {

        String sql_select = "Select * From users ";




        try(Connection conn = DBconnection.createNewDBconnection()){

            stmt = conn.createStatement();
            results = stmt.executeQuery(sql_select);
            List<Student> studentsList = new ArrayList<>();

            while (results.next()) {

                Student stdObject = new Student();
                stdObject.setId(Integer.valueOf(results.getString("id")));
                stdObject.setName(results.getString("name"));
                stdObject.setEmail(results.getString("email"));
                stdObject.setMobile_number(results.getString("mobile_number"));
                studentsList.add(stdObject);

            }

            ObjectMapper mapper = new ObjectMapper();
            String JSONOutput = mapper.writeValueAsString(studentsList);
            System.out.println(JSONOutput);


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
