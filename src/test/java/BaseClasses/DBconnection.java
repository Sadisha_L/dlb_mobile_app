package BaseClasses;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DBconnection {

   public static String driver = "com.mysql.jdbc.Driver";
    private static String dbhost = "jdbc:mysql://152.70.101.195:55306/elottery_test";
    private static String username = "root";
    private static String password = "Covid19StayStrongStayHome";
    private static Connection conn;



    @SuppressWarnings("finally")
    public static Connection createNewDBconnection() {
        try  {
            Class driver_class = Class.forName(driver);
            Driver driver = (Driver) driver_class.newInstance();
            DriverManager.registerDriver(driver);

            conn = DriverManager.getConnection(
                    dbhost, username, password);
        } catch (SQLException e) {
            System.out.println("Cannot create database connection");
            e.printStackTrace();
        } finally {
            return conn;
        }
    }
}
